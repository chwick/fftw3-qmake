include(fftw3_global.pri)

FFTW_LIB_PATH += -L$$FFTW_DESTDIR
FFTW_INCLUDE_PATHS += $$FFTW_ROOT/api

contains(FFTW_LIB_MODE_CONFIG, staticlib) {
    # there are circular dependencies, which is why we have to use the '{start/end}-group' flag on linux/mingw gcc/ld. No problems on msvc or darwin clang
    win32-g++:FFTW_EXTERN_LIBS += -Wl,--start-group -lfftw3 -ldftscalar -ldftscalarcodelets -ldft -lrdftscalar -lrdftscalarr2cf -lrdftscalarr2r -lrdftscalarr2cb -lrdft -lreodft -lkernel -Wl,--end-group
    else:linux-g++|android:FFTW_EXTERN_LIBS += -Xlinker -start-group -lfftw3 -ldftscalar -ldftscalarcodelets -ldft -lrdftscalar -lrdftscalarr2cf -lrdftscalarr2r -lrdftscalarr2cb -lrdft -lreodft -lkernel -Xlinker -end-group
    else:FFTW_EXTERN_LIBS += -lfftw3 -ldftscalar -ldftscalarcodelets -ldft -lrdftscalar -lrdftscalarr2cf -lrdftscalarr2r -lrdftscalarr2cb -lrdft -lreodft -lkernel

    FFTW_LIB_PATH += -L$$FFTW_SUBPART_OUT
} else {
    android:FFTW_EXTERN_LIBS += -lfftw3_$${ANDROID_TARGET_ARCH}
    else:FFTW_EXTERN_LIBS += -lfftw3

    FFTW_LIB_MODE_CONFIG = dll
    DEFINES += "FFTW_DLL=1"
}
