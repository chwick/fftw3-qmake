# default settings (these variables may be changed in fftw3_config.pri

# the destination dir for the fftw inclusive subparts
FFTW_DESTDIR = $$shadowed($$PWD)

# default build
ios:FFTW_LIBRARY_TYPE = static
else:FFTW_LIBRARY_TYPE = dll

# include config file if it exists
exists(fftw3_config.pri):include(fftw3_config.pri)

# library type
contains(FFTW_LIBRARY_TYPE, static) {
    FFTW_LIB_MODE_CONFIG = staticlib
} else {
    FFTW_LIB_MODE_CONFIG = dll
    DEFINES += "FFT_DLL=1"
}

# directory for subpart builds
FFTW_SUBPART_OUT = $$FFTW_DESTDIR/fftw_subparts

# root dir for the fftw code
FFTW_ROOT = $$PWD/fftw3

# root dir for the config files
FFTW_CONFIG_PATH = $$PWD

# additional include paths
android {
    INCLUDEPATH += $$PWD/android
}
else:linux {
    INCLUDEPATH += $$PWD/linux
}
else:ios {
    INCLUDEPATH += $$PWD/ios
}
else:macx {
    INCLUDEPATH += $$PWD/osx
}
else:win32 {
    INCLUDEPATH += $$PWD/windows
}
